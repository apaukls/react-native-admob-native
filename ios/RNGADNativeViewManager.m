#import "RNGADNativeViewManager.h"
#import "RNGADNativeView.h"

#if __has_include(<React/RCTBridge.h>)
#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTViewManager.h>
#else
#import "RCTBridge.h"
#import "RCTUIManager.h"
#import "RCTViewManager.h"
#import "RCTEventDispatcher.h"
#endif


@implementation RNGADNativeViewManager 

RCT_EXPORT_MODULE(RNGADNativeView);

RNGADNativeView *nativeAdView;

-(UIView *)view
{
    nativeAdView = [[RNGADNativeView alloc]initWithBridge:self.bridge];
    return nativeAdView;
}

RCT_EXPORT_VIEW_PROPERTY(nativeAdId, NSString)
RCT_EXPORT_VIEW_PROPERTY(adSize, NSString)
RCT_EXPORT_VIEW_PROPERTY(headline, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(tagline, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(advertiser, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(store, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(icon, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(image, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(mediaview, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(price, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(starrating, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(callToAction, NSNumber)


//RCT_EXPORT_VIEW_PROPERTY(ratingStarsColor, NSString)

RCT_EXPORT_VIEW_PROPERTY(onAdOpened, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAdClosed, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAdLeftApplication, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAdClicked, RCTBubblingEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onAdImpression, RCTBubblingEventBlock)

@end
