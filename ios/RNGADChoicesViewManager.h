//
//  RNGADChoicesViewManager.h
//  Pods
//
//  Created by Aliaksandr Paukalas on 5/13/20.
//

#if __has_include(<React/RCTViewManager.h>)
#import <React/RCTViewManager.h>
#else
#import "RCTViewManager.h"
#endif


#import "RCTViewManager.h"


@interface RNGADChoicesViewManager : RCTViewManager


@end

