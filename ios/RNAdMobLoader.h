//
//  RNAdMobLoader.h
//  Pods
//
//  Created by Aliaksandr Paukalas on 5/11/20.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@import GoogleMobileAds;

typedef void (^OnAdUpdated)(NSString *loaderId, NSArray<NSDictionary*> * ad);
typedef void (^OnAdError)(NSString *loaderId, NSError * error);

@interface RNAdMobLoader : NSObject

+ (id)sharedRNAdMobLoader;

- (GADNativeAd *) nativeAdById: (NSString *)adId;

- (void)loadAd:(NSDictionary *)params;

- (void)disposeAd:(NSArray<NSString *> *)ids;

- (void)addEventHandler:(OnAdUpdated)onLoaded
                onError: (OnAdError)onError;

@end

NS_ASSUME_NONNULL_END
