//
//  RNAdMobLoader.m
//  Pods
//
//  Created by Aliaksandr Paukalas on 5/11/20.
//

#import "RNAdMobLoader.h"

static NSString *const kAdMobLoaderIdParam = @"loaderId";
static NSString *const kAdMobAdUnitIdParam = @"adUnitId";
static NSString *const kAdMobAdNumberOfAdsParam = @"numberOfAds";

@import GoogleMobileAds;

@interface RNAdMobLoader()<GADNativeAdLoaderDelegate>

@property (nonatomic, strong) NSMutableDictionary<NSString *, GADAdLoader*> *loaders;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray<NSDictionary*>*> *ads;
@property (nonatomic, strong) NSMutableDictionary<NSString *, GADNativeAd*> *adsItems;

@property (nonatomic) OnAdUpdated onLoaded;
@property (nonatomic) OnAdError onError;

@end

@implementation RNAdMobLoader

+ (id) sharedRNAdMobLoader {
    static dispatch_once_t pred = 0;
    static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        self.loaders = [NSMutableDictionary dictionary];
        self.ads = [NSMutableDictionary dictionary];
        self.adsItems = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void)addEventHandler:(OnAdUpdated)onLoaded
                onError: (OnAdError)onError {
    self.onLoaded = onLoaded;
    self.onError = onError;
}

- (void)cleanAdsItems:(NSString *)loaderId {
    NSArray<NSDictionary*> *items = self.ads[loaderId];
    if (items) {
        self.ads[loaderId] = NULL;
        for (int i=0; i<items.count; i++) {
            self.adsItems[loaderId] = NULL;
        }
    }
}

- (GADAdLoader* )loaderForParams:(NSDictionary *)params {
    NSString *loaderId = params[kAdMobLoaderIdParam];
    GADAdLoader* adLoader = self.loaders[loaderId];
    if (adLoader && adLoader.isLoading) {
        return adLoader;
    }
    
    
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIViewController *rootViewController = [keyWindow rootViewController];
    
    GADNativeAdViewAdOptions *adViewOptions = [GADNativeAdViewAdOptions new];
    adViewOptions.preferredAdChoicesPosition = GADAdChoicesPositionTopRightCorner;
    
    NSString * adUnitId = params[kAdMobAdUnitIdParam];
    
    adLoader = [[GADAdLoader alloc]
                             initWithAdUnitID:adUnitId
                             rootViewController:rootViewController
                             adTypes:@[GADAdLoaderAdTypeNative]
                             options:@[adViewOptions]];
    
    self.loaders[loaderId] = adLoader;
    [self cleanAdsItems: loaderId];
    return adLoader;
}

- (void)loadAd:(NSDictionary *)params {
    GADAdLoader* adLoader = [self loaderForParams:params];
    if (!adLoader.isLoading) {
        adLoader.delegate = self;
        [adLoader loadRequest:[GADRequest request]];
    }
}

- (void)disposeAd:(NSArray<NSString *> *)ids {
    for (id loaderId in ids){
        self.loaders[loaderId] = NULL;
        [self cleanAdsItems:loaderId];
    }
}

- (GADNativeAd *) nativeAdById: (NSString *)adId {
    return self.adsItems[adId];
}

- (NSString*)idForLoader:(GADAdLoader *)adLoader {
    for (id key in self.loaders){
        id value = [self.loaders objectForKey:key];
        if ([value isEqual: adLoader]) {
            return key;
        }
    }
    return NULL;
}

- (void)adLoader:(GADAdLoader *)adLoader didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"GADAdLoader didFailToReceiveAdWithError: %@", [error localizedDescription]);
    NSString* loaderId = [self idForLoader:adLoader];
    self.onError(loaderId, error);
}

- (void)adLoader:(GADAdLoader *)adLoader didReceiveNativeAd:(GADNativeAd *)nativeAd {
    NSLog(@"adLoader didReceiveUnifiedNativeAd: %@", nativeAd.headline);
    
    NSString* loaderId = [self idForLoader:adLoader];
    if (nativeAd != NULL && loaderId != NULL) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        [dic setValue:nativeAd.headline forKey:@"headline"];
        [dic setValue:nativeAd.body forKey:@"tagline"];
        [dic setValue:nativeAd.advertiser forKey:@"advertiser"];
        [dic setValue:nativeAd.store forKey:@"store"];
        [dic setValue:nativeAd.price forKey:@"price"];
        [dic setValue:nativeAd.callToAction forKey:@"callToAction"];
        
        [dic setValue:@(nativeAd.mediaContent.hasVideoContent) forKey:@"video"];
        [dic setValue:@(nativeAd.mediaContent.aspectRatio) forKey:@"aspectRatio"];
        NSMutableArray *images = [NSMutableArray array];
        
        for (int i=0;i < nativeAd.images.count;i++) {
            NSMutableDictionary *imageDic = [NSMutableDictionary dictionary];
            GADNativeAdImage *image = [nativeAd.images objectAtIndex:i];
            [imageDic setValue:[image.imageURL absoluteString] forKey:@"url"];
            [imageDic setValue: @(image.image.size.width)  forKey:@"width"];
            [imageDic setValue: @(image.image.size.height) forKey:@"height"];
            [images addObject:imageDic];
        }
        
        [dic setObject:images forKey:@"images"];
        [dic setValue:[nativeAd.icon.imageURL absoluteString] forKey:@"icon"];
        [dic setValue:nativeAd.starRating forKey:@"rating"];
        NSMutableArray<NSDictionary*> *items = self.ads[loaderId];
        if (!items) {
            items = [NSMutableArray arrayWithCapacity:5];
            self.ads[loaderId] = items;
        }
        [items addObject:dic];
        

        [dic setValue:loaderId forKey:@"id"];
        self.adsItems[loaderId] = nativeAd;
        
        self.onLoaded(loaderId, items);
    }
}


@end

