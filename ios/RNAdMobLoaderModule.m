//
//  RNAdMobLoaderModule.m
//  Pods
//
//  Created by Aliaksandr Paukalas on 5/11/20.
//

#import "RNAdMobLoaderModule.h"
#import "RNAdMobLoader.h"

@implementation RNAdMobLoaderModule

RCT_EXPORT_MODULE()

- (dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

+ (BOOL)requiresMainQueueSetup {
    return YES;
}

- (NSArray<NSString *> *)supportedEvents {
  return @[@"NativeAdUpdated", @"NativeAdError"];
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [[RNAdMobLoader sharedRNAdMobLoader] addEventHandler:^(NSString * _Nonnull loaderId, NSArray<NSDictionary *> * _Nonnull ad) {
            [self sendEventWithName:@"NativeAdUpdated" body:@{@"loaderId": loaderId, @"items": ad}];
        } onError:^(NSString * _Nonnull loaderId, NSError * _Nonnull error) {
            [self sendEventWithName:@"NativeAdError" body:@{@"loaderId": loaderId, @"error": [error localizedDescription]}];
        }];
    }
    
    return self;
}

#pragma mark - Exported JS Functions

RCT_EXPORT_METHOD(loadAd:(NSDictionary *)params)
{
    [[RNAdMobLoader sharedRNAdMobLoader] loadAd:params];
}

RCT_EXPORT_METHOD(disposeAd:(NSArray<NSString *> *)ids)
{
    [[RNAdMobLoader sharedRNAdMobLoader] disposeAd:ids];
}

@end
