//
//  RNAdMobLoaderModule.h
//  Pods
//
//  Created by Aliaksandr Paukalas on 5/11/20.
//

#import <Foundation/Foundation.h>

#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#else
#import "RCTBridgeModule.h"
#import "RCTEventEmitter.h"
#endif


NS_ASSUME_NONNULL_BEGIN

@interface RNAdMobLoaderModule : RCTEventEmitter<RCTBridgeModule>

@end

NS_ASSUME_NONNULL_END
