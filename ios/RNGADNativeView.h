#import <React/RCTView.h>
#import <React/RCTBridge.h>

@import GoogleMobileAds;

@interface RNGADNativeView : GADNativeAdView <GADNativeAdDelegate>


@property (nonatomic, copy) NSString *nativeAdId;


@property (nonatomic, copy) NSNumber *headline;
@property (nonatomic, copy) NSNumber *tagline;
@property (nonatomic, copy) NSNumber *advertiser;
@property (nonatomic, copy) NSNumber *store;
@property (nonatomic, copy) NSNumber *price;
@property (nonatomic, copy) NSNumber *image;
@property (nonatomic, copy) NSNumber *icon;
@property (nonatomic, copy) NSNumber *mediaview;
@property (nonatomic, copy) NSNumber *starrating;
@property (nonatomic, copy) NSNumber *callToAction;
@property (nonatomic, copy) NSNumber *choicesview;

- (instancetype)initWithBridge:(RCTBridge *)bridge;

@property (nonatomic, copy) RCTBubblingEventBlock onAdOpened;
@property (nonatomic, copy) RCTBubblingEventBlock onAdClosed;
@property (nonatomic, copy) RCTBubblingEventBlock onAdLeftApplication;
@property (nonatomic, copy) RCTBubblingEventBlock onAdClicked;
@property (nonatomic, copy) RCTBubblingEventBlock onAdImpression;

@property (nonatomic, weak) RCTBridge *bridge;


@end
