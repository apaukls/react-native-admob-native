#import "RNGADNativeView.h"
#import "RNAdMobUtils.h"
#import <React/RCTRootView.h>
#import <React/RCTRootViewDelegate.h>
#import <React/RCTViewManager.h>
#import <React/RCTUtils.h>
#import <React/RCTAssert.h>
#import <React/RCTBridge.h>
#import <React/RCTConvert.h>
#import <React/RCTUIManager.h>
#import <React/RCTBridgeModule.h>
#import "RCTUIManagerUtils.h"
#import "RNAdMobLoader.h"

@import GoogleMobileAds;

@implementation RNGADNativeView : GADNativeAdView


- (instancetype)initWithBridge:(RCTBridge *)bridge {
    if (self = [super init]) {
        self.bridge = bridge;
    }
    return self;
}

- (void)setNativeAdId:(NSString *)nativeAdId {
    dispatch_async(RCTGetUIManagerQueue(),^{        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            GADNativeAd *nativeAd = [[RNAdMobLoader sharedRNAdMobLoader] nativeAdById:nativeAdId];
            nativeAd.delegate = self;
            [self setNativeAd:nativeAd];
            [self setupMediaView];
        }];
    });
}

- (void)setHeadline:(NSNumber *)headline {
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *headlineView = viewRegistry[headline];
            
            if (headlineView != nil) {
                headlineView.userInteractionEnabled = NO;
                [self setHeadlineView:headlineView];
            }
        }];
    });
}

- (void)setIcon:(NSNumber *)icon {
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *iconView = viewRegistry[icon];
            if (iconView != nil) {
                iconView.userInteractionEnabled = NO;
                [self setIconView:iconView];
            }
        }];
    });
    
    
}

- (void)setImage:(NSNumber *)image {
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *imageView = viewRegistry[image];
            if (imageView != nil) {
                imageView.userInteractionEnabled = NO;
                [self setImageView:imageView];
            }
        }];
    });
    
}

- (void)setMediaview:(NSNumber *)mediaview
{
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            GADMediaView *mediaView = (GADMediaView *) viewRegistry[mediaview];
            if (mediaView != nil) {
                [self setMediaView:mediaView];
                [self setupMediaView];
            }
        }];
    });
}

- (void)setTagline:(NSNumber *)tagline
{
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *taglineView = viewRegistry[tagline];
            if (taglineView != nil) {
                taglineView.userInteractionEnabled = NO;
                [self setPriceView:taglineView];
            }
        }];
    });
    
}

- (void)setAdvertiser:(NSNumber *)advertiser
{
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *advertiserView = viewRegistry[advertiser];
            if (advertiserView != nil) {
                advertiserView.userInteractionEnabled = NO;
                [self setAdvertiserView:advertiserView];
            }
        }];
    });
    
}
- (void)setPrice:(NSNumber *)price
{
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *priceView = viewRegistry[price];
            if (priceView != nil) {
                priceView.userInteractionEnabled = NO;
                [self setPriceView:priceView];
            }
        }];
    });
}

- (void)setStore:(NSNumber *)store
{
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *storeView = viewRegistry[store];
            if (storeView != nil) {
                storeView.userInteractionEnabled = NO;
                [self setStoreView:storeView];
            }
        }];
    });
}

- (void)setStarrating:(NSNumber *)starrating
{
    
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *starratingView = viewRegistry[starrating];
            if (starratingView != nil) {
                starratingView.userInteractionEnabled = NO;
                [self setStarRatingView:starratingView];
            }
        }];
    });
    
}

- (void)setCallToAction:(NSNumber *)callToAction
{
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *callToActionView = viewRegistry[callToAction];
            
            if (callToActionView != nil){
                callToActionView.userInteractionEnabled = NO;
                [self setCallToActionView:callToActionView];
            }
            
        }];
    });
}

- (void)setChoicesview:(NSNumber *)choicesview
{
    dispatch_async(RCTGetUIManagerQueue(),^{
        
        [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
            
            UIView *view = viewRegistry[choicesview];            
            if (view != nil){
                [self setAdChoicesView:(GADAdChoicesView *)view];
            }
        }];
    });
}


- (void)nativeAdDidRecordImpression:(nonnull GADNativeAd *)nativeAd
{
    if (self.onAdImpression) {
        self.onAdImpression(@{});
    }
}

- (void)nativeAdDidRecordClick:(nonnull GADNativeAd *)nativeAd
{
    if (self.onAdClicked) {
        self.onAdClicked(@{});
    }
}

- (void)nativeAdWillPresentScreen:(nonnull GADNativeAd *)nativeAd
{
    if (self.onAdOpened) {
        self.onAdOpened(@{});
    }
}

- (void)nativeAdWillDismissScreen:(nonnull GADNativeAd *)nativeAd
{
    if (self.onAdClosed) {
        self.onAdClosed(@{});
    }
}

- (void)nativeAdWillLeaveApplication:(nonnull GADNativeAd *)nativeAd
{
    if(self.onAdLeftApplication) {
        self.onAdLeftApplication(@{});
    }
}

- (void)setupMediaView {
    GADNativeAd *nativeAd = [[RNAdMobLoader sharedRNAdMobLoader] nativeAdById:self.nativeAdId];
    if (nativeAd) {
        self.mediaView.mediaContent = nativeAd.mediaContent;
    }
}

@end




