package com.ammarahmed.rnadmob.nativeads;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

public class RNAdLoaderModule extends ReactContextBaseJavaModule {

    public RNAdLoaderModule(@NonNull ReactApplicationContext reactContext) {
        super(reactContext);
        RNAdLoader.createLoader(reactContext);
    }

    static final String EVENT_NATIVE_AD_UPDATED = "NativeAdUpdated";
    static final String EVENT_NATIVE_AD_ERROR = "NativeAdError";

    @NonNull
    @Override
    public String getName() {
        return "RNAdMobLoaderModule";
    }

    @ReactMethod
    public void loadAd(ReadableMap params) {
        RNAdLoader.getInstance().loadAd(params);
    }

    @ReactMethod
    public void disposeAd(ReadableArray ids) {
        RNAdLoader.getInstance().dispose(ids);
    }
}
