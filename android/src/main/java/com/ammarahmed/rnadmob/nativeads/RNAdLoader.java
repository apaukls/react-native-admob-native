package com.ammarahmed.rnadmob.nativeads;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RNAdLoader {

    private static final String PARAM_LOADER_ID = "loaderId";
    private static final String PARAM_AD_UNIT_ID = "adUnitId";
    private static final String PARAM_NUMBER_OF_ADS = "numberOfAds";

    private static final String PARAM_ERROR = "error";
    private static final String PARAM_ITEMS = "items";

    private static RNAdLoader ourInstance;

    static RNAdLoader getInstance() {
        return ourInstance;
    }

    static void createLoader(ReactContext context) {
        if (ourInstance == null)
            ourInstance = new RNAdLoader(context);
    }

    private final ReactContext context;
    private final Map<String, AdLoader> loaders;
    private final Map<String, AdListener> listeners;
    private final Map<String, NativeAd> adsItems;


    private RNAdLoader(ReactContext context) {
        this.loaders = new HashMap<>();
        this.adsItems = new HashMap<>();
        this.listeners = new HashMap<>();
        this.context = context;
    }

    public NativeAd nativeAdById(String id) {
        return this.adsItems.get(id);
    }

    public void addListener(String id, AdListener listener) {
        listeners.put(id, listener);
    }

    public void removeListener(String id, AdListener listener) {
        if (listener.equals(listeners.get(id)) )
            listeners.remove(id);
    }

    void loadAd(ReadableMap params) {
        AdLoader loader = loaderWithParams(params);
        if (!loader.isLoading()) {
            loader.loadAd(new AdRequest.Builder().build());
        }
    }

    void dispose(ReadableArray ids) {
        for (Object id : ids.toArrayList()) {
            cleanAdsItems((String) id);
        }
    }

    private void cleanAdsItems(String loaderId) {
        NativeAd ad = adsItems.get(loaderId);
        if (ad != null) {
            ad.destroy();
            adsItems.remove(loaderId);
        }
    }

    private AdLoader loaderWithParams(ReadableMap params) {
        final String loaderId = params.getString(PARAM_LOADER_ID);
        if (loaderId != null && loaders.containsKey(loaderId)) {
            AdLoader loader = loaders.get(loaderId);
            if (loader.isLoading()) {
                return loader;
            }
        }

        cleanAdsItems(loaderId);

        String adUnitId = params.getString(PARAM_AD_UNIT_ID);

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(true)
                .build();

        AdLoader.Builder builder = new AdLoader.Builder(this.context, adUnitId)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(@NonNull NativeAd nativeAd) {
                        if (nativeAd != null) {


                            Bundle convertedAd = convertNativeAdToJS(nativeAd);
                            convertedAd.putString("id", loaderId);
                            List<Bundle> list = new ArrayList<>(1);
                            list.add(convertedAd);

                            RNAdLoader.this.adsItems.put(loaderId, nativeAd);


                            WritableMap event = Arguments.createMap();
                            event.putString(PARAM_LOADER_ID, loaderId);
                            event.putArray(PARAM_ITEMS, Arguments.fromList(list));

                            RNAdLoader.this.sendEvent(RNAdLoaderModule.EVENT_NATIVE_AD_UPDATED, event);
                        }
                    }
                }).withAdListener(new AdListener() {
                    public void onAdFailedToLoad(@NonNull LoadAdError adError) {
                        String errorMessage = "Unknown error";
                        switch (adError.getCode()) {
                            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                                errorMessage = "Internal error, an invalid response was received from the ad server.";
                                break;
                            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                                errorMessage = "Invalid ad request, possibly an incorrect ad unit ID was given.";
                                break;
                            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                                errorMessage = "The ad request was unsuccessful due to network connectivity.";
                                break;
                            case AdRequest.ERROR_CODE_NO_FILL:
                                errorMessage = "The ad request was successful, but no ad was returned due to lack of ad inventory.";
                                break;
                        }

                        WritableMap error = Arguments.createMap();
                        error.putString(PARAM_ERROR, errorMessage);
                        error.putString(PARAM_LOADER_ID, loaderId);
                        RNAdLoader.this.sendEvent(RNAdLoaderModule.EVENT_NATIVE_AD_ERROR, error);
                    }

                    public void onAdOpened() {
                        AdListener listener = RNAdLoader.this.listeners.get(loaderId);
                        if (listener != null) {
                            listener.onAdOpened();
                        }
                    }

                    public void onAdClicked() {
                        AdListener listener = RNAdLoader.this.listeners.get(loaderId);
                        if (listener != null) {
                            listener.onAdClicked();
                        }
                    }

                    public void onAdImpression() {
                        AdListener listener = RNAdLoader.this.listeners.get(loaderId);
                        if (listener != null) {
                            listener.onAdImpression();
                        }
                    }

                }).withNativeAdOptions(new NativeAdOptions.Builder()
                        .setVideoOptions(videoOptions)
                        .setAdChoicesPlacement(NativeAdOptions.ADCHOICES_TOP_RIGHT)
                        .build());

        AdLoader loader = builder.build();
        loaders.put(loaderId, loader);

        return loader;
    }

    private Bundle convertNativeAdToJS(NativeAd nativeAd) {

        Bundle args = new Bundle();
        args.putString("headline", nativeAd.getHeadline());
        args.putString("tagline", nativeAd.getBody());
        args.putString("advertiser", nativeAd.getAdvertiser());
        args.putString("callToAction", nativeAd.getCallToAction());
        args.putBoolean("video", nativeAd.getMediaContent().hasVideoContent());
        args.putString("price", nativeAd.getPrice());
        if (nativeAd.getStore() != null) {
            args.putString("store", nativeAd.getStore());
        }
        if (nativeAd.getStarRating() != null) {
            args.putInt("rating", nativeAd.getStarRating().intValue());
        }
        args.putString("aspectRatio", String.valueOf(nativeAd.getMediaContent().getAspectRatio()));
        ArrayList<Bundle> images = new ArrayList<>(nativeAd.getImages().size());

        for (int i = 0; i < nativeAd.getImages().size(); i++) {
            Bundle map = new Bundle();
            map.putString("url", nativeAd.getImages().get(i).getUri().toString());
            Drawable d = nativeAd.getImages().get(i).getDrawable();
            if (d != null) {
                map.putInt("width", d.getIntrinsicWidth());
                map.putInt("height", d.getIntrinsicHeight());
            } else {
                map.putInt("width", 0);
                map.putInt("height", 0);
            }

            images.add(map);
        }

        args.putParcelableArrayList("images", images);
        if (nativeAd.getIcon() != null && nativeAd.getIcon().getUri() != null) {
            args.putString("icon", nativeAd.getIcon().getUri().toString());
        }


        return args;
    }

    private void sendEvent(String name, @Nullable WritableMap event) {
        this.context.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(name, event);
    }
}
