package com.ammarahmed.rnadmob.nativeads;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.nativead.AdChoicesView;
import com.google.android.gms.ads.nativead.NativeAd;

import java.util.Map;


public class RNAdMobNativeViewManager extends ViewGroupManager<RNNativeAdWrapper> {


    private static final String REACT_CLASS = "RNGADNativeView";


    static final String EVENT_UNIFIED_NATIVE_AD_LOADED = "onUnifiedNativeAdLoaded";


    private static final String PROP_NATIVE_AD_ID = "nativeAdId";

    private static final String PROP_HEADLINE_VIEW = "headline";
    private static final String PROP_TAGLINE_VIEW = "tagline";
    private static final String PROP_ADVERTISER_VIEW = "advertiser";
    private static final String PROP_STORE_VIEW = "store";
    private static final String PROP_PRICE_VIEW = "price";
    private static final String PROP_IMAGE_VIEW = "image";
    private static final String PROP_ICON_VIEW = "icon";
    private static final String PROP_MEDIA_VIEW = "mediaview";
    private static final String PROP_STAR_RATING_VIEW = "starrating";
    private static final String PROP_CALL_TO_ACTION_VIEW = "callToAction";
    private static final String PROP_CHOICES_VIEW = "choicesview";

    static final String EVENT_AD_OPENED = "onAdOpened";
    static final String EVENT_AD_CLOSED = "onAdClosed";
    static final String EVENT_AD_LEFT_APPLICATION = "onAdLeftApplication";
    static final String EVENT_AD_IMPRESSION = "onAdImpression";
    static final String EVENT_AD_CLICKED = "onAdClicked";


    private RNNativeAdWrapper rnNativeAdWrapper;
    private ThemedReactContext reactContext;
    private NativeAd ad;
    private String adId;


    @javax.annotation.Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        MapBuilder.Builder<String, Object> builder = MapBuilder.builder();
        String[] events = new String[]{
                EVENT_AD_CLICKED,
                EVENT_AD_CLOSED,
                EVENT_AD_OPENED,
                EVENT_AD_IMPRESSION,
                EVENT_AD_LEFT_APPLICATION,
                EVENT_UNIFIED_NATIVE_AD_LOADED
        };
        for (String event : events) {
            builder.put(event, MapBuilder.of("registrationName", event));
        }
        return builder.build();
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected RNNativeAdWrapper createViewInstance(ThemedReactContext reactContext) {
        this.reactContext = reactContext;
        rnNativeAdWrapper = new RNNativeAdWrapper(reactContext);
        return rnNativeAdWrapper;
    }

    @Override
    public void addView(RNNativeAdWrapper parent, View child, int index) {
        rnNativeAdWrapper.addNewView(child, index);
    }

    private AdListener adListener = new AdListener() {
        @Override
        public void onAdImpression() {
            sendEvent(EVENT_AD_IMPRESSION, null);
        }
    };

    @ReactProp(name = PROP_NATIVE_AD_ID)
    public void setNativeAdId(final RNNativeAdWrapper v, final String adId) {
        NativeAd ad = RNAdLoader.getInstance().nativeAdById(adId);
        if (ad != null) {
            if (this.adId != null) {
                RNAdLoader.getInstance().removeListener(this.adId, adListener);
            }
            this.adId = adId;
            this.ad = ad;
            RNAdLoader.getInstance().addListener(adId, adListener);
            rnNativeAdWrapper.nativeAdView.setNativeAd(ad);
        }
    }

    @ReactProp(name = PROP_MEDIA_VIEW)
    public void setMediaView(final RNNativeAdWrapper view, final int id) {
        rnNativeAdWrapper.addMediaView(id);
        if (this.ad != null) {
            rnNativeAdWrapper.nativeAdView.setNativeAd(ad);
        }
    }

    @ReactProp(name = PROP_HEADLINE_VIEW)
    public void setHeadlineView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null)
            rnNativeAdWrapper.nativeAdView.setHeadlineView(view);
    }

    @ReactProp(name = PROP_TAGLINE_VIEW)
    public void setPropTaglineView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setBodyView(view);
    }

    @ReactProp(name = PROP_ADVERTISER_VIEW)
    public void setPropAdvertiserView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setAdvertiserView(view);
    }

    @ReactProp(name = PROP_IMAGE_VIEW)
    public void setPropImageView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setImageView(view);
    }

    @ReactProp(name = PROP_ICON_VIEW)
    public void setPropIconView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setIconView(view);
    }

    @ReactProp(name = PROP_STORE_VIEW)
    public void setPropStoreView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setStoreView(view);
    }

    @ReactProp(name = PROP_PRICE_VIEW)
    public void setPropPriceView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setPriceView(view);
    }

    @ReactProp(name = PROP_STAR_RATING_VIEW)
    public void setPropStarRatingView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setStarRatingView(view);

    }

    @ReactProp(name = PROP_CALL_TO_ACTION_VIEW)
    public void setPropCallToActionView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setCallToActionView(view);
    }

    @ReactProp(name = PROP_CHOICES_VIEW)
    public void setPropChoicesView(final RNNativeAdWrapper v, final int id) {
        View view = rnNativeAdWrapper.findViewById(id);
        if (view != null && rnNativeAdWrapper.nativeAdView != null)
            rnNativeAdWrapper.nativeAdView.setAdChoicesView((AdChoicesView) view);
    }

    @Override
    public void onDropViewInstance(@NonNull RNNativeAdWrapper view) {
        super.onDropViewInstance(view);
        rnNativeAdWrapper.removeHandler();
    }


    private void sendEvent(String name, @Nullable WritableMap event) {
        this.reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
                rnNativeAdWrapper.getId(),
                name,
                event);
    }

}
