package com.ammarahmed.rnadmob.nativeads;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.nativead.NativeAdView;

public class RNNativeAdWrapper extends LinearLayout {

    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };


    NativeAdView nativeAdView;
    private Handler handler;


    public RNNativeAdWrapper(Context context) {
        super(context);
        createView(context);
        handler = new Handler();
    }

    public void createView(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewRoot = layoutInflater.inflate(R.layout.rn_ad_unified_native_ad, this, true);
        nativeAdView = viewRoot.findViewById(R.id.native_ad_view);
    }

    public void addMediaView(int id) {
        RNMediaView adMediaView = nativeAdView.findViewById(id);
        if (adMediaView != null) {
            nativeAdView.setMediaView(adMediaView);
            adMediaView.requestLayout();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void addNewView(View child, int index) {
        nativeAdView.addView(child, index);
        requestLayout();
        nativeAdView.requestLayout();
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        requestLayout();
    }

    @Override
    public void requestLayout() {
        super.requestLayout();
        post(measureAndLayout);
    }


    public void removeHandler() {
        if (handler != null) {
            handler.removeCallbacks(null);
            handler = null;
        }
    }
}
