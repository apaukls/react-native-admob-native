import React, { createRef, useEffect, useContext } from "react";
import { findNodeHandle, requireNativeComponent } from "react-native";
import { NativeAdContext } from "./context";

const ChoicesView = (props) => {
  const { nativeAdView } = useContext(
    NativeAdContext
  );
  const view = createRef();

  const _onLayout = () => {
    if (!nativeAdView) return;
    let handle = findNodeHandle(view.current);
    nativeAdView.setNativeProps({
      choicesview: handle,
    });
  };

  useEffect(() => {
    _onLayout();
  }, [nativeAdView]);

  return (
    <AdChoicesView ref={view} onLayout={_onLayout} style={props.style} />
  );
};

const AdChoicesView = requireNativeComponent("ChoicesView", ChoicesView);

export default ChoicesView;
