import React, { useState } from "react";
import { NativeModules, NativeEventEmitter, requireNativeComponent, Platform, View } from "react-native";
import { NativeAdContext } from "./context";

const NativeAdView = (props) => {
  const [nativeAdView, setNativeAdView] = useState(null);
  let nativeAdRef;

  const _onAdClicked = (event) => {
    if (props.onAdClicked) props.onAdClicked(event.nativeEvent);
  };

  const _onAdImpression = (event) => {
    if (props.onAdImpression) props.onAdImpression(event.nativeEvent);
  };

  const _onAdClosed = (event) => {
    if (props.onAdClosed) props.onAdClosed(event.nativeEvent);
  };

  const _onAdOpened = (event) => {
    if (props.onAdOpened) props.onAdOpened(event.nativeEvent);
  };

  const _onAdLefApplication = (event) => {
    if (props.onAdLeftApplication) props.onAdLeftApplication(event.nativeEvent);
  };

  const childer = Platform.OS === "android" ? (
    <View collapsable={false}>{props.children}</View>
  ) : props.children

  return (
    <NativeAdContext.Provider
      value={{ nativeAdView, setNativeAdView }}
    >
      <UnifiedNativeAdView
        ref={(ref) => {
          nativeAdRef = ref;
          setNativeAdView(nativeAdRef);
          return nativeAdRef;
        }}

        onAdClicked={_onAdClicked}
        onAdLeftApplication={_onAdLefApplication}
        onAdOpened={_onAdOpened}
        onAdClosed={_onAdClosed}
        onAdImpression={_onAdImpression}
        style={props.style}
        nativeAdId={props.nativeAdId}
      >
        {childer}
      </UnifiedNativeAdView>
    </NativeAdContext.Provider>
  );
};

NativeAdView.simulatorId = "SIMULATOR";

const UnifiedNativeAdView = requireNativeComponent(
  "RNGADNativeView",
  NativeAdView
);

export function loadAd(params) {
  return NativeModules.RNAdMobLoaderModule.loadAd(params)
}

export function disposeAd(ids) {
  return NativeModules.RNAdMobLoaderModule.disposeAd(ids)
}

const eventEmitter = new NativeEventEmitter(NativeModules.RNAdMobLoaderModule);

export function addOnUpdatedListener(loaderId, listener) {
  return eventEmitter.addListener('NativeAdUpdated',
    (ads) => ads.loaderId === loaderId && listener(ads.items), undefined)
}

export function addOnErrorListener(loaderId, listener) {
  return eventEmitter.addListener('NativeAdError',
    (error) => error.loaderId === loaderId && listener(error.error), undefined)
}

export default NativeAdView;
