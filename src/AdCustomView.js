import React, { createRef, useContext, useEffect } from "react";
import { findNodeHandle, View } from "react-native";
import { NativeAdContext } from "./context";

const AdCustomView = (props) => {
  const { nativeAdView } = useContext(
    NativeAdContext
  );
  const viewRef = createRef();

  const _onLayout = () => {
    if (!nativeAdView) return;
    let handle = findNodeHandle(viewRef.current);
    nativeAdView.setNativeProps({
      [props.adViewId]: handle,
    });
  };

  useEffect(() => {
    _onLayout();
  }, [nativeAdView]);

  return (
    <View
      style={props.style}
      ref={viewRef}
      onLayout={_onLayout}>
      {props.children}
    </View>
  );
};

export default AdCustomView;
