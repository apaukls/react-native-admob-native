import { ViewStyle, StyleProp, EmitterSubscription } from "react-native";
import * as React from "react";

type Image = {
  /**
   * The url of the image
   */
  url: string;
  /**
   * The height of the image
   */
  height: number;
  /**
   * The width of the image
   */
  width: number;
}


type NativeAd = {
  /**
   * Id of native Ad content
   */
  id: string;

  /**
   * Title of the native ad.
   */
  headline: string;

  /**
   * Description of the native ad.
   */
  tagline: string;
  /**
   * Advertiser of the native ad.
   */
  advertiser: string;
  /**
   * If the ad is for a paid app or service, price value.
   */
  price: string;

  /**
   * Aspect ratio of the Content loaded inside MediaView.
   * You should use this to calculate the correct width and height of the MediaView.
   */
  aspectRatio:number;
  /**
   * If ad is for an app, then name of the store.
   */
  store: string;
  /**
   * Icon / Logo of the adveriser or the product advertised in ad.
   */
  icon: string;
  /**
   * An array of images along with the ad.
   */
  images: Array<Image>;
  /**
   * callToAction text value for the native ad.
   */
  callToAction: string;
  /**
   * If ad is for an app, then its rating on the respective store.
   */
  rating: number;

  /**
   * if ad has video content or not.
   */
  video: boolean;
};

type SimpleViewProps = {
  style?: StyleProp<ViewStyle>;
};

type NativeAdViewProps = {
  nativeAdId: string
  onAdOpened?: Function;
  onAdClosed?: Function;
  onAdLeftApplication?: Function;
  onAdImpression?: Function;
  onAdClicked?: Function;
  children: React.ReactNode
} & SimpleViewProps;

type NativeAdParams = {
  loaderId: string;
  adUnitId: string;
  numberOfAds?: number;
};

type AdViewType = 'headline' | 'tagline' | 'advertiser' | 'store' | 'price' |
  'image' | 'icon' | 'starrating' | 'callToAction' | 'choicesview'

type CustomAdViewProps = {
  adViewId: AdViewType;
  children: React.ReactNode
} & SimpleViewProps;


declare module "react-native-admob-native" {

  export function loadAd(params: NativeAdParams);

  export function disposeAd(ids: string[]);

  export function addOnUpdatedListener(loaderId: string,
                                       listener: (items: NativeAd[]) => void): EmitterSubscription;

  export function addOnErrorListener(loaderId: string,
                                     listener: (error: string) => void): EmitterSubscription;

  export default class NativeAdView extends React.Component<NativeAdViewProps> {}

  export class AdCustomView extends React.Component<CustomAdViewProps> {}

  export class ChoicesView extends React.Component {}

  export class MediaView extends React.Component<SimpleViewProps> {}
}
